<?php

namespace App\Http\Controllers;
use Auth;
use App\Tag;
use App\Category;
use App\Post;


use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function index()
    {
        return view('admin.posts.index')->with('posts', Post::all());
    }

    public function create()
    {
        $categories = Category::all();
        if($categories->count() == 0)
        {
            Session::flash('info', 'You Must have Choose At least One Category');

            return redirect()->back();
        }
        return view('admin.posts.create')->with('categories',$categories)
                                         ->with('tags',Tag::all());
    }
    public function store(Request $request)
    {
        $this->validate($request,[

            'title' => 'required',
            'featured' => 'required|image',
            'body' => 'required',
            'category_id'=> 'required',
             'tags'=>'required'
            

        ]);

        $featured = $request->featured;
        $featuerd_new = time().$featured->getClientoriginalName();
        $featured->move('uploads/posts', $featuerd_new);

        // $post = Post::create([
        //         'title' => $request->title,
        //         'body' => $request->body,
        //         'featured' => 'uploads/posts/'. $featuerd_new,
        //         'category_id' => $request->category_id,
        //         'slug'=> $request->title,
        //         'user_id'=>Auth::id()


        // ]);

        $post = new Post();
        $post->title = $request->title;
        $post->body = $request->body;
        $post->featured = 'uploads/posts/'. $featuerd_new;
        $post->category_id = $request->category_id;
        $post->slug= $request->title;
        $post->user_id = Auth::user()->id;
        $post->save();
        $post->tags()->attach($request->tags);
       return redirect()->route('posts');
    }

    // public function show($id)
    // {
    //     //
    // }

   
    public function edit($id)
    {
        $posts = Post::find($id);

        return view('admin.posts.edit')->with('posts',$posts)

                                       ->with('categories', Category::all())
                                       ->with('tags',Tag::all());
    }

    
    public function update(Request $request, $id)
    {
        

         $this->validate($request,[

            'title' => 'required',
            'body' => 'required',
            'category_id'=> 'required'
            

        ]);

        $posts = Post::find($id);

        if($request->hasfile('featured')){


        $featured = $request->featured;
        $featuerd_new = time().$featured->getClientoriginalName();
        $featured->move('uploads/posts', $featuerd_new);
        $posts->featured ='uploads/posts/'.$featuerd_new;

        }


         $posts->title = $request->title;
         $posts->body = $request->body;
         $posts->category_id = $request->category_id;
         $posts->save();
         $posts->tags()->sync($request->tags);

      
       Session::flash('success', 'You succesfully updated a Post.');
        return redirect()->route('posts');
       



    }

    public function destroy($id)
    {
        $posts = Post::find($id);
        $posts->delete();
        return redirect()->back();
    }

    public function trashed()
    {
 
       $posts = Post::onlyTrashed()->get();
       return view('admin.posts.trashed')->with('posts', $posts);

    }

    public function kill($id)
    {
       $posts = Post::withTrashed()->where('id',$id)->first();
       $posts->forceDelete();
       return redirect()->back();
      
    }

    public function restore($id)

    {
        $posts = Post::withTrashed()->where('id',$id)->first();
        $posts->restore();
        return redirect()->route('posts');


    }
}
