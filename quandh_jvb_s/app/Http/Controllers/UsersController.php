<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;

class UsersController extends Controller
{

    public function __construct()
    {
       $this->middleware('admin');

    }

    public function index()
    {
        return view('admin.users.index')->with('users', User::all());
    }
    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {
         $this->validate($request,[

                'name' => 'required',
                'email' => 'required|email'

        ]);

          $user = User::create([

             'name' => $request->name,
             'email' => $request->email,
             'password' => bcrypt('password')

          ]);

          return redirect()->route('users');
    }
    public function destroy($id)
    {
         $user = User::find($id);
        $user->delete();

        return redirect()->back();
    }
    public function admin($id)
    {
        $user = User::find($id);
        $user->admin = 1;
        $user->save();
        return redirect()->back();

     

    } 
     public function not_admin($id)
    {
        $user = User::find($id);
        $user->admin = 0;
        $user->save();
        return redirect()->back();
    }
}
