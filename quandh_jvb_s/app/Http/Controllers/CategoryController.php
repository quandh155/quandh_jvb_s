<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;


class CategoryController extends Controller
{
    public function index()
    {
        return view('admin.categories.index')->with('categories',Category::all());
    }
    public function create()
    {
        return view('admin.categories.create');

    }
    public function store(Request $request)
    {
        $this->validate($request,[

                'name' => 'required'

        ]);

        $category = new Category();
        $category->name = $request->name;
        $category->save();
        return redirect()->route('categories');


    }
    public function edit($id)
    {
        $category = Category::find($id);
       
    
        return view('admin.categories.edit')->with('category', $category);
    }

    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request->name;
        $category->save();
        return redirect()->route('categories');
    }
    
    public function destroy($id)
    {
        $category = Category::find($id);

      foreach ($category->posts as $post) {
          

          $post->forceDelete();
      }
        $category->delete();
        return redirect()->back();
    }
}
